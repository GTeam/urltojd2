# Versions

## 2.1 (2021-12-30)
- fix the (undefined) url send to JD2 when text selection

## 2.0 (2021-12-20)
- Large Refactoring
- context menu and pageAction can send to Linkgrabber or Download JD2 list.
- remove "options" page

## 1.1 (2019-08-26)
- change (be better) the urlConstruct function for URL encode.

## 1.0 (2019-08-16)
- first release

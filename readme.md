# UrlToJD2 - A Firefox extension
Push an url from the browser or a link (right-click) to the download manager JDownloader 2.

This addons use the remote control API (externInterface: Flashgot) from JDownloader2.

## Prerequisites
Install [JDownloader2](http://beta.jdownloader.org/).
In "advanced parameters" search "RemoteAPI".
The value of "Authorized Website" must look like: ["127.0.0.1", "localhost"]
JD2 is now ready to listen !

## Usage
This extension add two things:
- A "pageAction" (a clickable icon inside the browser's address bar).
- An item in the context menu

With url or link (or link/url in selection), choose to send to the Linkgrabber or to the Download list.

## Note
If you want a more complete addons with JD2, try [the official one:](https://my.jdownloader.org/apps/)

## Code Source
[UrlToJD2](https://framagit.org/GTeam/urltojd2)

## Acknowledgments
- Extension icon: [primofenax](https://www.deviantart.com/primofenax/art/icon-Minimal-JDownloader-Icon-313625363)
- Context icons from JDownloader2

## Licence
This project is licensed under the Mozilla Public License, version 2.0

